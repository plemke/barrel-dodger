﻿using System.ComponentModel.DataAnnotations;

namespace ConstructionAlerts.Models
{
    public class Alert
    {
        [Required]
        [Display(Name = "Alert name")]
        public string AlertName { get; set; }
        [Display(Name = "Start Address")]
        public string startLocation { get; set; }
        [Display(Name = "End Address")]
        public string endLocation { get; set; }
        [Display(Name = "Alert name")]
        public string range { get; set; }
    }
}
