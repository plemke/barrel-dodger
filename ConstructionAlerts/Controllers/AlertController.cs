﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConstructionAlerts.Models;

namespace ConstructionAlerts.Controllers
{
    public class AlertController : Controller
    {
        //
        // GET: /Alert/
        public ActionResult Index()
        {
            List<Alert> alerts = new List<Alert>();

            Alert test = new Alert();
            test.AlertName = "My WorK Route";
            test.range = "5 miles";

            Alert test2 = new Alert();
            test2.AlertName = "My route to school";
            test2.range = "5 miles";
            alerts.Add(test2);

            Alert test3 = new Alert();
            test3.AlertName = "Around my house";
            test3.range = "5 miles";
            alerts.Add(test3);

            Alert test4 = new Alert();
            test4.AlertName = "Around the Ice Hogs";
            test4.range = "5 miles";
            alerts.Add(test4);

            alerts.Add(test);
            return View(alerts);
        }

        //
        // GET: /Alert/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Alert/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Alert/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Alert/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Alert/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Alert/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Alert/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
